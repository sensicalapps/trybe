//
//  TRRootVC.m
//  Trybe
//
//  Created by Daniel James on 6/1/13.
//  Copyright (c) 2013 Trybe. All rights reserved.
//

#import "TRRootVC.h"
#import "TRMenuTVC.h"

@interface TRRootVC ()

@end

@implementation TRRootVC

- (id)init
{
    self = [super init];
    if (self) {
        //do stuffs

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.paperFoldContainer = [[PaperFoldView alloc] initWithFrame:CGRectMake(0, 0, [self.view bounds].size.width, [self.view bounds].size.height)];
    [self.paperFoldContainer setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
    [self.view addSubview:self.paperFoldContainer];
    
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.paperFoldContainer action:@selector(onContentViewPanned:)];
    [self.searchServicesNC.navigationBar addGestureRecognizer:panGestureRecognizer];
    
    self.menuVC = [[TRMenuTVC alloc] initWithNibName:@"TRMenuTVC" bundle:nil];
    self.menuVC.rootVC = self;
    
    [self.paperFoldContainer setLeftFoldContentView:self.menuVC.tableView];
    [self.paperFoldContainer setCenterContentView:self.searchServicesNC.view];
    
    self.paperFoldContainer.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark Nav Controllers and 
- (TRUserProfileVC *)userProfileTV
{
    if (!_userProfileTV) {
        _userProfileTV = [[TRUserProfileVC alloc] initWithNibName:@"TRUserProfileVC" bundle:nil];
        _userProfileTV.title = @"User Profile";
        _userProfileTV.rootVC = self;
    }
    return _userProfileTV;
}

- (UINavigationController *)userProfileNC
{
    if (!_userProfileNC) {
        _userProfileNC = [[UINavigationController alloc] initWithRootViewController:self.userProfileTV];
        _userProfileNC.view.frame = CGRectMake(0, 0, [self.view bounds].size.width, [self.view bounds].size.height);
        
        UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.paperFoldContainer action:@selector(onContentViewPanned:)];
        [_userProfileNC.navigationBar addGestureRecognizer:panGestureRecognizer];
    }
    
    self.userProfileTV.currentNavController = _userProfileNC;
    self.userProfileTV.paperFoldContainer = self.paperFoldContainer;

    return _userProfileNC;
}

- (TRSearchServicesTVC *)searchServicesTV
{
    if (!_searchServicesTV) {
        _searchServicesTV = [[TRSearchServicesTVC alloc] initWithNibName:@"TRSearchServicesTVC" bundle:nil];
        _searchServicesTV.title = @"Summon Your Tribe";
        _searchServicesTV.rootVC = self;
    }
    return _searchServicesTV;
}

- (UINavigationController *)searchServicesNC
{
    if (!_searchServicesNC) {
        _searchServicesNC = [[UINavigationController alloc] initWithRootViewController:self.searchServicesTV];
        _searchServicesNC.view.frame = CGRectMake(0, 0, [self.view bounds].size.width, [self.view bounds].size.height);
        
        UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.paperFoldContainer action:@selector(onContentViewPanned:)];
        [_searchServicesNC.navigationBar addGestureRecognizer:panGestureRecognizer];
    }
    
    self.searchServicesTV.currentNavController = _searchServicesNC;
    self.searchServicesTV.paperFoldContainer = self.paperFoldContainer;
    
    return _searchServicesNC;
}

- (TRConnectionsTVC *)connectionsTV
{
    if (!_connectionsTV) {
        _connectionsTV = [[TRConnectionsTVC alloc] initWithNibName:@"TRConnectionsTVC" bundle:nil];
        _connectionsTV.title = @"Build Tribe";
        _connectionsTV.rootVC = self;
    }
    return _connectionsTV;
}

- (UINavigationController *)connectionsNC
{
    if (!_connectionsNC) {
        _connectionsNC = [[UINavigationController alloc] initWithRootViewController:self.connectionsTV];
        _connectionsNC.view.frame = CGRectMake(0, 0, [self.view bounds].size.width, [self.view bounds].size.height);
        
        UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.paperFoldContainer action:@selector(onContentViewPanned:)];
        [_connectionsNC.navigationBar addGestureRecognizer:panGestureRecognizer];
    }
    
    self.connectionsTV.currentNavController = _connectionsNC;
    self.connectionsTV.paperFoldContainer = self.paperFoldContainer;
    
    return _connectionsNC;
}


#pragma mark Set Content View Handlers

- (void)displayProfileNC
{
    if (self.paperFoldContainer.contentView != self.userProfileNC.view) {
        [self.paperFoldContainer.contentView.subviews.lastObject removeFromSuperview];
        [self.paperFoldContainer setCenterContentView:self.userProfileNC.view];
    }
    [self.paperFoldContainer setPaperFoldState:PaperFoldStateDefault];
}

- (void)displaySearchNC
{
    if (self.paperFoldContainer.contentView != self.searchServicesNC.view) {
        [self.paperFoldContainer.contentView.subviews.lastObject removeFromSuperview];
        [self.paperFoldContainer setCenterContentView:self.searchServicesNC.view];
    }
    [self.paperFoldContainer setPaperFoldState:PaperFoldStateDefault];
}

- (void)displayConnectionsNC
{
    if (self.paperFoldContainer.contentView != self.connectionsNC.view) {
        [self.paperFoldContainer.contentView.subviews.lastObject removeFromSuperview];
        [self.paperFoldContainer setCenterContentView:self.connectionsNC.view];
    }
    [self.paperFoldContainer setPaperFoldState:PaperFoldStateDefault];
}


#pragma mark - PaperFoldView Delegate
- (void)paperFoldView:(id)paperFoldView didFoldAutomatically:(BOOL)automated toState:(PaperFoldState)paperFoldState
{
    DNSLog(@"did transition to state %i", paperFoldState);
}

#pragma mark - Action Handlers
- (void)openLeftPaneView
{
    switch (self.paperFoldContainer.state) {
        case PaperFoldStateDefault:
            [self.paperFoldContainer setPaperFoldState:PaperFoldStateLeftUnfolded];
            break;
        case PaperFoldStateLeftUnfolded:
            [self.paperFoldContainer setPaperFoldState:PaperFoldStateDefault];
            break;
        default:
            [self.paperFoldContainer setPaperFoldState:PaperFoldStateDefault];
            break;
    }
}


@end
