//
//  TRUserProfileVC.h
//  Trybe
//
//  Created by Daniel James on 6/1/13.
//  Copyright (c) 2013 Trybe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaperFoldView.h"
@class TRRootVC;

@interface TRUserProfileVC : UIViewController

@property (strong, nonatomic) UINavigationController *currentNavController;
@property (strong, nonatomic) PaperFoldView *paperFoldContainer;
@property (strong, nonatomic) TRRootVC *rootVC;

@property (nonatomic) NSInteger *userID;
@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *emailAddress;
@property (strong, nonatomic) IBOutlet UILabel *phoneNumber;
@property (strong, nonatomic) IBOutlet UILabel *city;
@property (strong, nonatomic) IBOutlet UIView *userCardView;
@property (strong, nonatomic) IBOutlet UIButton *addService;

@end
