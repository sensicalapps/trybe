//
//  TRAppDelegate.m
//  Trybe
//
//  Created by Daniel James on 6/1/13.
//  Copyright (c) 2013 Trybe. All rights reserved.
//

#import "TRAppDelegate.h"
#import "TRRootVC.h"


@implementation TRAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [MagicalRecord setupAutoMigratingCoreDataStack];
    [self customizeAppearance];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.rootVC = [[TRRootVC alloc] init];

    self.window.rootViewController = self.rootVC;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)customizeAppearance
{
    UIColor *orangeColor = [UIColor colorWithRed:0.91 green:0.49 blue:0.001 alpha:1.0];
    UIColor *greyColor   = [UIColor colorWithRed:0.93 green:0.92 blue:0.89 alpha:1.0];
    UIColor *brownColor  = [UIColor colorWithRed:0.54 green:0.29 blue:0.125 alpha:1.0];
    
    //[[UINavigationBar appearance] setTintColor: brownColor];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navbarbrown"] forBarMetrics:UIBarMetricsDefault];
    
    NSMutableDictionary *textAttributes;
    [textAttributes setObject:greyColor forKey:UITextAttributeTextColor];
    [[UINavigationBar appearance] setTitleTextAttributes:textAttributes];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [MagicalRecord cleanUp];
}

@end
