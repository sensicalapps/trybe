//
//  main.m
//  Trybe
//
//  Created by Daniel James on 6/1/13.
//  Copyright (c) 2013 SW. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TRAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TRAppDelegate class]));
    }
}
