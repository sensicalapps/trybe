//
//  TRRootVC.h
//  Trybe
//
//  Created by Daniel James on 6/1/13.
//  Copyright (c) 2013 Trybe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaperFoldView.h"

@class TRMenuTVC;
#import "TRUserProfileVC.h"
#import "TRSearchServicesTVC.h"
#import "TRConnectionsTVC.h"

@interface TRRootVC : UIViewController <PaperFoldViewDelegate>

// ONE VIEW TO RULE THEM ALL!!!
@property (strong, nonatomic) PaperFoldView *paperFoldContainer;

@property (strong, nonatomic) TRMenuTVC *menuVC;
@property (strong, nonatomic) UINavigationController *userProfileNC, *searchServicesNC, *connectionsNC;
@property (strong, nonatomic) TRUserProfileVC *userProfileTV;
@property (strong, nonatomic) TRSearchServicesTVC *searchServicesTV;
@property (strong, nonatomic) TRConnectionsTVC *connectionsTV;

- (void)displayProfileNC;
- (void)displaySearchNC;
- (void)displayConnectionsNC;

@end
