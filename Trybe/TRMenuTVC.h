//
//  TRMenuTVC.h
//  Trybe
//
//  Created by Daniel James on 6/1/13.
//  Copyright (c) 2013 Trybe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRRootVC.h"

@interface TRMenuTVC : UITableViewController

@property (strong, nonatomic) TRRootVC *rootVC;
@property (strong, nonatomic) NSArray *cellInfo;    //data source 


@end
