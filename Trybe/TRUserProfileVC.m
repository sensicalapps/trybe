//
//  TRUserProfileVC.m
//  Trybe
//
//  Created by Daniel James on 6/1/13.
//  Copyright (c) 2013 Trybe. All rights reserved.
//

#import "TRUserProfileVC.h"
#import "TRRootVC.h"


@interface TRUserProfileVC ()

@end

@implementation TRUserProfileVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    //do stuffs
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //UIBarButtonItem *openButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self.rootVC action:@selector(openLeftPaneView)];
    UIButton *menuButtonView = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButtonView setBackgroundImage:[UIImage imageNamed:@"menubutton"] forState:UIControlStateNormal];
    menuButtonView.frame = CGRectMake(1, 1, 30, 30);
    [menuButtonView addTarget:self.rootVC action:@selector(openLeftPaneView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithCustomView:menuButtonView];
    self.navigationItem.leftBarButtonItem = menuButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    UIColor *orangeColor = [UIColor colorWithRed:0.91 green:0.49 blue:0.001 alpha:1.0];
    UIColor *greyColor   = [UIColor colorWithRed:0.93 green:0.92 blue:0.89 alpha:1.0];
    UIColor *brownColor  = [UIColor colorWithRed:0.54 green:0.29 blue:0.125 alpha:1.0];
    
    
    self.userCardView.backgroundColor = greyColor;
//    self.userName.textColor = greyColor;
//    self.emailAddress.textColor = greyColor;
//    self.phoneNumber.textColor = greyColor;
//    self.city.textColor = greyColor;
    //self.addService.backgroundColor = orangeColor;
    
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    return cell;
}

#pragma mark HTTP Request


@end
