//
//  TRSearchServicesTVC.h
//  Trybe
//
//  Created by Daniel James on 6/1/13.
//  Copyright (c) 2013 Trybe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaperFoldView.h"
@class TRRootVC;

@interface TRSearchServicesTVC : UITableViewController

@property (strong, nonatomic) UINavigationController *currentNavController;
@property (strong, nonatomic) PaperFoldView *paperFoldContainer;
@property (strong, nonatomic) TRRootVC *rootVC;

@end
